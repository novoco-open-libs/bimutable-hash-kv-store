# Bimutable Hash Kv Store

In interface for kv stores that use cryptographic hashes for keys
For immutable data the key is a hash of the value
For mutable data the key is a hash of the public key of the owner of the data and the data is signed by the owners private key