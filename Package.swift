// swift-tools-version:5.2
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "BimutableHashKVStore",
    platforms: [
        .macOS(.v10_15),
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "BimutableHashKVStore",
            targets: ["BimutableHashKVStore"]
        ),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        .package(
            name: "Cache",
            url: "https://github.com/hyperoslo/Cache.git",
            .upToNextMinor(from: "6.0.0")
        ),
        .package(
            name: "CryptoSwift",
            url: "https://github.com/krzyzanowskim/CryptoSwift.git",
            .upToNextMinor(from: "1.3.8")
        ),
        .package(
            name: "SwiftMultihash",
            url: "https://gitlab.com/novoco-open-libs/swift-multihash.git",
            .branch("master")
        ),
        .package(
            name: "Flynn",
            url: "https://github.com/novocodev/flynn.git",
            .branch("master")
        ),
        .package(
            name: "Cuckoo",
            url: "https://github.com/Brightify/Cuckoo.git",
            .upToNextMajor(from: "1.4.0")
        ),
        .package(
            name: "SwiftTasks",
            url: "https://gitlab.com/novoco-open-libs/swift-tasks.git",
            .branch("master")
        ),
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "BimutableHashKVStore",
            dependencies: ["CryptoSwift", "Flynn", "SwiftMultihash", "Cache"]
        ),
        .testTarget(
            name: "BimutableHashKVStoreTests",
            dependencies: ["CryptoSwift", "Flynn", "Cuckoo", "BimutableHashKVStore"]
        ),
    ]
)
