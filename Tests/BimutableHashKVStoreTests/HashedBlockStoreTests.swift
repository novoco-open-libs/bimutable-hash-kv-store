import XCTest
import Flynn
import CryptoSwift
import SwiftMultihash

@testable import BimutableHashKVStore

private struct TestHashedBlockHanlderState: HashedBlockHandlerState {
    
    let expectation: XCTestExpectation
    func handleBlock(_ hash: Multihash, _ value: [UInt8]) {
        print(">>>>>>>>>>>>> TestHashedBlockHanlderState.handleBlock(...) called with ",value)
        expectation.fulfill()
    }

    public init (_ expectation: XCTestExpectation) {
        self.expectation =  expectation
    }
}

private class BlockHandlerOne : Actor, HashedBlockHandler {
    public var safeState: HashedBlockHandlerState
    
    public init (_ expectation: XCTestExpectation) {
        self.safeState = TestHashedBlockHanlderState(expectation)
    }
}

final class HashedBlockStoreTests: XCTestCase {
    func testStoreBlockInCache() {
        
        let expectation = XCTestExpectation(description: "A block is stored and the handler called")
        
        HashedBlockStore().beStoreBlock([0], BlockHandlerOne(expectation))
        wait(for: [expectation], timeout: 10.0)
        
        Flynn.shutdown()
    }
    
    func testStoreAndGetBlockInCache() {
        
        let cleartext: [UInt8] = [1]
        let storeexpectation = XCTestExpectation(description: "A block is stored and the handler called")
        let getexpectation = XCTestExpectation(description: "A block is retrieved and the handler called")
        
        let hbs: HashedBlockStore = HashedBlockStore()
        hbs.beStoreBlock(cleartext, BlockHandlerOne(storeexpectation))

        wait(for: [storeexpectation], timeout: 10.0)
        
        let digest = cleartext.sha512()
        
        var pre = [0,0] as [UInt8]
        
        pre[0] = UInt8(SHA2_512)
        pre[1] = UInt8(digest.count)
        pre.append(contentsOf: digest)
         
        hbs.beGetBlock(Multihash(pre), BlockHandlerOne(getexpectation))
        
        wait(for: [getexpectation], timeout: 10.0)
        
        Flynn.shutdown()
    }

    static var allTests = [
        ("testStoreBlockInMemory", testStoreBlockInCache),
        ("testStoreAndGetBlockInMemory", testStoreAndGetBlockInCache),
    ]
}
