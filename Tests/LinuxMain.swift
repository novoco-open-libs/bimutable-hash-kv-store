import XCTest

import BimutableHashKVStoreTests

var tests = [XCTestCaseEntry]()
tests += BimutableHashKVStoreTests.allTests()
XCTMain(tests)
