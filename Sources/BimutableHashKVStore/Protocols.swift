//
//  File.swift
//  
//
//  Created by novoco dev on 06/10/2020.
//

import Foundation
import SwiftMultihash

protocol SignedBlockRefMap {
    @discardableResult
    func beSignBlockRef(_ block: Multihash, _ publicKey: Int, _ handler: SignedBlockRefHandler ) -> Self
    
    @discardableResult
    func beBlockRefStream(_ blockref: Multihash, _ handler: BlockRefStreamHandler ) -> Self
}


protocol SignedBlockRefHandler {
    @discardableResult
    func beHandleBlockRef(_ key: Multihash, _ value: Any?) -> Self
}

protocol BlockRefStreamHandler {
    @discardableResult
    func beHandleBlockRefStream(_ key: Multihash, _ value: Any?) -> Self
}

