import Flynn
import SwiftMultihash

public protocol HashedBlockStoreProvider {
    @discardableResult
    func beGetBlock(_ hash: Multihash, _ handler: HashedBlockHandler ) -> Self
    
    @discardableResult
    func beStoreBlock(_ hash: [UInt8], _ value: [UInt8], _ handler: HashedBlockHandler ) -> Self
}
